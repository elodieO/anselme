//
//  AppDelegate.swift
//  Anselme
//
//  Created by digital on 10/02/2019.
//  Copyright © 2019 Elodie Oudot. All rights reserved.
//

import UIKit
import UserNotifications
import Firebase
import FirebaseInstanceID
import FirebaseMessaging

let MessageOptionKey = "MessageOption"
let ReceivedMessageOptionKey = "ReceivedMessageOption"

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate, MessagingDelegate {

    var window: UIWindow?
    var productCommunicationManager = ProductCommunicationManager()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        self.productCommunicationManager.registerWithSDK()
        UserDefaults.standard.register(defaults: [MessageOptionKey: MessageOption.noLineEnding.rawValue,
                                                  ReceivedMessageOptionKey: ReceivedMessageOption.none.rawValue])
        
        setupActions()
        registerForNotifications(application)
//        if #available(iOS 10.0, *) {
//            // For iOS 10 display notification (sent via APNS)
//            UNUserNotificationCenter.current().delegate = self
//            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
//            UNUserNotificationCenter.current().requestAuthorization(
//                options: authOptions,
//                completionHandler: {_, _ in })
//            // For iOS 10 data message (sent via FCM
//
//            Messaging.messaging().delegate = self
//
//
//
//        } else {
//
//            let settings: UIUserNotificationSettings =
//                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
//            application.registerUserNotificationSettings(settings)
//        }
//
//        application.registerForRemoteNotifications()
//
//        FirebaseApp.configure()
        
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        let vc = storyboard.instantiateViewController(withIdentifier: "waitingRoom")
//        window?.rootViewController = vc
//
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("Error fetching remote instance ID: \(error)")
            } else if let result = result {
                print("Remote instance ID token: \(result.token)")
            }
        }
       
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    func applicationReceivedRemoteMessage(_ remoteMessage: MessagingRemoteMessage) {
        print(remoteMessage.appData)
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler:
        @escaping () -> Void) {
        
        print("notif ! ")
        // Perform the task associated with the action.
        switch response.actionIdentifier {
        case "available":
            print("available")
            SocketIOManager.shared.connectSocket()
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let myVC = storyboard.instantiateViewController(withIdentifier: "ResponseView") as! ResponseViewController
            myVC.test = "yo"
            window?.rootViewController = myVC
        
            break
            
        case "occuped":
            print("occuped")
//            sharedMeetingManager.declineMeeting(user: userID,
//                                                meetingID: meetingID)
            break
            
            // Handle other actions…
            
        default:
            SocketIOManager.shared.connectSocket()
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let myVC = storyboard.instantiateViewController(withIdentifier: "ResponseView") as! ResponseViewController
            myVC.test = "yo"
            window?.rootViewController = myVC
            break
        }
        
        // Always call the completion handler when done.
        completionHandler()
    }
    
    func registerForNotifications(_ application: UIApplication) {
        
        UNUserNotificationCenter.current().delegate = self
        
        let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
        
        UNUserNotificationCenter.current().requestAuthorization(options: authOptions) { (granted, error) in
            
            print("Permission granted: \(granted)")
            
        }
        
        application.registerForRemoteNotifications ()
    }
    
    func setupActions(){
        let firstAction = UNNotificationAction( identifier: "available", title: "Je suis disponible !", options: [.foreground])
        let secondAction = UNNotificationAction( identifier: "occuped", title: "Une autre fois...", options: UNNotificationActionOptions(rawValue: 0))
        
        let generalCategory = UNNotificationCategory(identifier: "myCategory",
                                                     actions: [firstAction, secondAction],
                                                     intentIdentifiers: [],
                                                     options: [])
        // Register the category.
        let center = UNUserNotificationCenter.current()
        center.setNotificationCategories([generalCategory])
    }

}

