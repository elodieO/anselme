//
//  LandingManager.swift
//  Anselme
//
//  Created by digital on 10/02/2019.
//  Copyright © 2019 Elodie Oudot. All rights reserved.
//

import Foundation
import DJISDK

class EventManager {
    static let instance = EventManager()
    var takePhotoFirstCallback:(()->())? = nil
    var takePhotoSecondCallback:(()->())? = nil
    
    /*
     * Landing the drone
     */
    func landing() {
        if ConfigManager.shared.config["debug"] == "true" {
            print("*** Landing ***")
        } else {
            if let mySpark = DJISDKManager.product() as? DJIAircraft {
                if let flightController = mySpark.flightController {
                    flightController.startLanding(completion: { (err) in
                        print(err.debugDescription)
                    })
                }
            }
        }
    }
    
    /*
     * takeOff the drone
     */
    func takeOff() {
        if ConfigManager.shared.config["debug"] == "true" {
            print("*** Take Off ***")
        } else {
            if let mySpark = DJISDKManager.product() as? DJIAircraft {
                if let flightController = mySpark.flightController {
                    flightController.startTakeoff(completion: { (err) in
                        print(err.debugDescription)
                    })
                }
            }
        }
    }
    
    /* To stop spark */
    func stop() {
        if let mySpark = DJISDKManager.product() as? DJIAircraft {
            mySpark.mobileRemoteController?.rightStickVertical = 0.0
            mySpark.mobileRemoteController?.rightStickHorizontal = 0.0
            mySpark.mobileRemoteController?.leftStickVertical = 0.0
            mySpark.mobileRemoteController?.leftStickHorizontal = 0.0
        }
    }
    
    /* To stop spark */
    func emergencyStop() {
        print("emergency stop")
        SequenceManager.instance.restart()
        if let mySpark = DJISDKManager.product() as? DJIAircraft {
            mySpark.mobileRemoteController?.rightStickVertical = 0.0
            mySpark.mobileRemoteController?.rightStickHorizontal = 0.0
            mySpark.mobileRemoteController?.leftStickVertical = 0.0
            mySpark.mobileRemoteController?.leftStickHorizontal = 0.0
            
        }
    }
    
    /* To execute sparkDirectionVertical actions */
    func executeSparkEvent(action : ActionSparkEvent) {
        
        if ConfigManager.shared.config["debug"] == "true" {
            print(action.event)
            print("action")
        } else {
            switch action.event {
            case .landing:
                landing()
            case .takeOff:
                takeOff()
            case .stop:
                stop()
            }
        }
        
    }
    
    /* To execute sparkDirectionVertical actions */
    func executeGimbalEvent(action : ActionGimbalEvent) {
        
        if ConfigManager.shared.config["debug"] == "true" {
            print(action.event)
            print("action")
        } else {
            switch action.event {
            case .takePictureFirst:
                print("send take picture callback to first !")
                self.takePhotoFirstCallback?()
            case .takePictureSecond:
                print("send take picture callback to second!")
                self.takePhotoSecondCallback?()
            }
        }
        
    }
    
}
