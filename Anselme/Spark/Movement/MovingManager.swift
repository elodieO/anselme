//
//  MovingManager.swift
//  Anselme
//
//  Created by digital on 17/02/2019.
//  Copyright © 2019 Elodie Oudot. All rights reserved.
//

import UIKit
import DJISDK

class MovingManager {
    
    static let instance = MovingManager()
    
    /* To execute sparkDirectionHorizontal actions */
    func executeSparkDirectionHorizontal(action : ActionSparkDirectionHorizontal) {
        
        if ConfigManager.shared.config["debug"] == "true" {
            print(action.direction)
            print("action with a speed of")
            print(action.speed)
        } else {
        
            if let mySpark = DJISDKManager.product() as? DJIAircraft {
                switch action.direction {
                case .left,.right:
                        mySpark.mobileRemoteController?.rightStickVertical = 0.0
                        mySpark.mobileRemoteController?.rightStickHorizontal = Float(action.direction.value().x * action.speed)
                
                case .forward,.backward:
                    mySpark.mobileRemoteController?.rightStickHorizontal = 0.0
                    mySpark.mobileRemoteController?.rightStickVertical = Float(action.direction.value().y * action.speed)
                    
                case .forwardLeft,.forwardRight, .backwardLeft, .backwardRight:
                    mySpark.mobileRemoteController?.rightStickHorizontal = Float(action.direction.value().x * action.speed)
                    mySpark.mobileRemoteController?.rightStickVertical = Float(action.direction.value().y * action.speed)
                    
                }
            }
        }

    }
    
    /* To execute sparkDirectionVertical actions */
    func executeSparkDirectionVertical(action : ActionSparkDirectionVertical) {
        
        if ConfigManager.shared.config["debug"] == "true" {
            print(action.direction)
            print("action with a speed of")
            print(action.speed)
        } else {
            if let mySpark = DJISDKManager.product() as? DJIAircraft {
                switch action.direction {
                case .top,.bottom:
                    mySpark.mobileRemoteController?.leftStickVertical = Float(action.direction.value() * action.speed)
                }
            }
        }
        
    }
    
    /* To execute sparkDirectionVertical actions */
    func executeSparkRotation(action : ActionSparkRotation) {
        
        if ConfigManager.shared.config["debug"] == "true" {
            print(action.rotation)
            print("action with a speed of")
            print(action.speed)
        } else {
            if let mySpark = DJISDKManager.product() as? DJIAircraft {
                switch action.rotation {
                case .clockwise,.counterClockwise:
                    mySpark.mobileRemoteController?.leftStickHorizontal = Float(action.rotation.value() * action.speed)
                    rotate(rotation: action.rotation.value())
                }
            }
        }
        
    }
    
    func directionHorizontal(direction: CGFloat){
        if let mySpark = DJISDKManager.product() as? DJIAircraft {
            mySpark.mobileRemoteController?.rightStickHorizontal = 0.0
            mySpark.mobileRemoteController?.rightStickVertical = Float(direction * 0.3)
        }
    }
    
    func rotate(rotation : CGFloat){
        if let mySpark = DJISDKManager.product() as? DJIAircraft {
            mySpark.mobileRemoteController?.leftStickHorizontal = Float(rotation * 0.5)
        }
    }

}
    

