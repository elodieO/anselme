//
//  BatteryManager.swift
//  Anselme
//
//  Created by digital on 02/03/2019.
//  Copyright © 2019 Elodie Oudot. All rights reserved.
//

import Foundation
import DJISDK

class BatteryManager {
    static let shared = BatteryManager()
    
    var updateBatteryInfos:((String)->())? = nil

    func getBatteryLevel() {
        if ConfigManager.shared.config["debug"] == "true" {
            print("*** Landing ***")
        } else {
            if let mySpark = DJISDKManager.product() as? DJIAircraft {
                if let battery = mySpark.battery {
                    battery.getCellVoltages(completion: { (values, err) in
                        // do what you need with the value
                        if let value = values?[0]{
                        }
                    })
                }
            }
        }
    }
}
