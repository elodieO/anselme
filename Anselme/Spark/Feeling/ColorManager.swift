//
//  ColorManager.swift
//  Anselme
//
//  Created by digital on 18/02/2019.
//  Copyright © 2019 Elodie Oudot. All rights reserved.
//

import Foundation
import DJISDK

class ColorManager {
    static let instance = ColorManager()
    
    func sendColorToBLE(color : String) {
        print("Sending \(color) to arduino")
        BluetoothManager.shared.sendMessageToSerial(msg: color)
    }
    
    /* To execute sparkColor actions */
    func executeSparkColor(action : ActionSparkColor) {

            switch action.BLEColor {
            case .angryRed: sendColorToBLE(color: "angry")
            case .happyRainbow: sendColorToBLE(color: "happy")
            case .sadBlue: sendColorToBLE(color: "sad")
            case .heartPink: sendColorToBLE(color: "heart")
            case .tiredPurple: sendColorToBLE(color: "tired")
            case .smilingYellow: sendColorToBLE(color: "smiling")
            case .yes: sendColorToBLE(color: "yes")
            case .no: sendColorToBLE(color: "no")
            case .reflecting: sendColorToBLE(color: "reflecting")
            case .understood: sendColorToBLE(color: "understood")
            case .repeating: sendColorToBLE(color: "repeating")
            case .wiz: sendColorToBLE(color: "wiz")
            case .offLeds: sendColorToBLE(color: "offLeds")
            }

        
    }
    
}

