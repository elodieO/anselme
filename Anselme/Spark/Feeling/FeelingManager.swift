//
//  FeelingManager.swift
//  Anselme
//
//  Created by digital on 20/02/2019.
//  Copyright © 2019 Elodie Oudot. All rights reserved.
//

import Foundation
import DJISDK

class FeelingManager {
    static let instance = FeelingManager()
    
    func executeFeelingSequence(fileName : String) {
        print("*** create scenario in feelingManager \(fileName)")
        let scenario = ScenarioManager(name: fileName)
        scenario.play()
    }
    
    /* To execute sparkColor actions */
    func executeSparkFeeling(actionName : String) {
        print("*** execute spark feeling in feelingManager \(actionName)")
        executeFeelingSequence(fileName: actionName)
    }
    
}

