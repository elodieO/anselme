//
//  GimbalManager.swift
//  Anselme
//
//  Created by digital on 10/02/2019.
//  Copyright © 2019 Elodie Oudot. All rights reserved.
//

import UIKit
import DJISDK

public class GimbalManager {
    
    var gimbal: DJIGimbal?
    var front:Float = 90.0
    var under:Float = -90.0
    var defaultPitch: NSNumber = 0
    private var speed: TimeInterval?
    var ready = false
    
    static let shared = GimbalManager()
    
    func setup() {
        gimbal = getGimbal()
        
        gimbal?.setMode(DJIGimbalMode.free, withCompletion: { (err) in
            self.ready = true
        })
        
    }
    
    private func getGimbal() -> DJIGimbal? {
        if let spark = DJISDKManager.product() as? DJIAircraft {
            if let gimbal = spark.gimbal {
                gimbal.delegate = self as? DJIGimbalDelegate
                
                return gimbal
            }
            
            return nil
        }
        
        return nil
    }
    
    func executeGimbalRotation(action : ActionGimbalRotation){
        
        if ConfigManager.shared.config["debug"] == "true" {
            print("\(action.rotation) action with a speed of \(action.speed)")
        } else {
            if let mySpark = DJISDKManager.product() as? DJIAircraft {
                switch action.rotation {
                case .down:
                    rotate(degrees: action.rotation.value(), speed: action.speed)
                case .up:
                    rotate(degrees: action.rotation.value(), speed: action.speed)
                }
                
            
            }
        }
        
    }
    
    
    func rotate(degrees: Float, speed: CGFloat) {
        
        let rotation = DJIGimbalRotation(pitchValue: NSNumber(value: degrees), rollValue: 0, yawValue: 0, time: TimeInterval(speed), mode: DJIGimbalRotationMode.relativeAngle)
        
        self.gimbal!.rotate(with: rotation, completion: { err in
            if err != nil {
                print("Error while rotating gimbal : \(String(describing: err))")
            }
        })
        
    }
    
    func resetGimbal(){
        self.gimbal!.reset { (err) in
            
        }
    }
}
