//
//  BluetoothManager.swift
//  Anselme
//
//  Created by digital on 16/02/2019.
//  Copyright © 2019 Elodie Oudot. All rights reserved.
//

import Foundation
import CoreBluetooth
import QuartzCore

enum MessageOption: Int {
    case noLineEnding,
    newline,
    carriageReturn,
    carriageReturnAndNewline
}

enum ReceivedMessageOption: Int {
    case none,
    newline
}

class BluetoothManager :NSObject, BluetoothSerialDelegate {
    
    static let shared = BluetoothManager()
    
    var peripherals: [(peripheral: CBPeripheral, RSSI: Float)] = []
    var selectedPeripheral: CBPeripheral?
    
    var didDeviceConnectedCallback:(()->())? = nil
    
    override init() {
        super.init()
        serial = BluetoothSerial(delegate: self)
    }
    
    func deviceConnection(){
        if serial.connectedPeripheral == nil {
            serial.startScan()
        } else {
            serial.disconnect()
        }
    }
    
    func scanTimeOut() {
        serial.stopScan()
    }
    
    func serialDidDiscoverPeripheral(_ peripheral: CBPeripheral, RSSI: NSNumber?) {
        // check whether it is a duplicate
        for exisiting in peripherals {
            if exisiting.peripheral.identifier == peripheral.identifier { return }
        }
        
        // add to the array, next sort & reload
        let theRSSI = RSSI?.floatValue ?? 0.0
        peripherals.append((peripheral: peripheral, RSSI: theRSSI))
        peripherals.sort { $0.RSSI < $1.RSSI }
        print(peripherals)
        serial.stopScan()
        selectedPeripheral = peripherals[0].peripheral
        serial.connectToPeripheral(selectedPeripheral!)
        
    }
    
    func serialDidFailToConnect(_ peripheral: CBPeripheral, error: NSError?) {
        print("serial failed to connect")
    }
    
    
    func serialIsReady(_ peripheral: CBPeripheral) {
        print("serial is ready")
        self.didDeviceConnectedCallback?()
        
        let pref = UserDefaults.standard.integer(forKey: MessageOptionKey)
        var msg = "a"
        switch pref {
        case MessageOption.newline.rawValue:
            msg += "\n"
        case MessageOption.carriageReturn.rawValue:
            msg += "\r"
        case MessageOption.carriageReturnAndNewline.rawValue:
            msg += "\r\n"
        default:
            msg += ""
        }
        
        sendMessageToSerial(msg: msg)
        
    }
    
    func serialDidChangeState() {
        print("serial did change state")
    }
    
    func serialDidDisconnect(_ peripheral: CBPeripheral, error: NSError?) {
        print("serial did disconenct")
        deviceConnection()
//        serial.startScan()
        
    }
    
    func sendMessageToSerial(msg : String){
        serial.sendMessageToDevice(msg)
    }


}
