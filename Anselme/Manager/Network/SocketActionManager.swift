//
//  SocketMoveManager.swift
//  Anselme
//
//  Created by digital on 19/02/2019.
//  Copyright © 2019 Elodie Oudot. All rights reserved.
//

import SocketIO
import UIKit

class SocketActionManager :NSObject {
    
    static let shared = SocketActionManager()
    
    func returnActions(){
        SocketIOManager.shared.actionSparkDirectionHorizontalRequestCallback = { (actionName) -> () in
        
            
            switch actionName {
            case ".forward":
                MovingManager.instance.directionHorizontal(direction: 1.0)
//                let action = Action.sparkDirectionHorizontal(direction: .forward, speed: 0.1)
//                MovingManager.instance.executeSparkDirectionHorizontal(action: action.actionValues as! ActionSparkDirectionHorizontal)
            default:
                EventManager.instance.stop()
            }
        }
        
        SocketIOManager.shared.actionSparkRotationRequestCallback = { (actionName) -> () in
            switch actionName {
            case ".clockwise":
//                let action = Action.sparkRotation(rotation: .clockwise, speed: 0.5)
//                MovingManager.instance.executeSparkRotation(action: action.actionValues as! ActionSparkRotation)
                MovingManager.instance.rotate(rotation: 90.0)
            case ".counterClockwise":
//                let action = Action.sparkRotation(rotation: .counterClockwise, speed: 0.5)
//                MovingManager.instance.executeSparkRotation(action: action.actionValues as! ActionSparkRotation)
                MovingManager.instance.rotate(rotation: -90.0)
            default:
                EventManager.instance.stop()
            }
        }
        
        SocketIOManager.shared.gimbalRotationRequestCallback = { (gimbalValue) -> () in
            let myFloat = (gimbalValue as NSString).floatValue
            GimbalManager.shared.rotate(degrees: myFloat, speed: 0.7)
        }
        
        SocketIOManager.shared.actionSparkEventRequestCallback = { (actionName) -> () in
            switch actionName {
            case ".stop":
                EventManager.instance.stop()
            case ".landing":
                EventManager.instance.landing()
            case ".takeOff":
                EventManager .instance.takeOff()
            case ".emergencyStop":
                EventManager.instance.emergencyStop()
            default:
                EventManager.instance.stop()
            }
        }
    }
    
    func returnFeelings(label : UILabel, image: UIImageView){
        SocketIOManager.shared.actionSparkFeelingRequestCallback = { (actionName) -> () in
            image.image = UIImage(named: "big-"+actionName)
            
            UIView.animate(withDuration: 1.0, delay: 0.0, options: UIView.AnimationOptions.curveEaseIn, animations: {
                image.alpha = 1.0
            }, completion: nil)
            
            FeelingManager.instance.executeSparkFeeling(actionName: actionName)
            
            SequenceManager.instance.sequenceFinishedCallback = { () -> () in
                SocketIOManager.shared.feelingsFinished()
                UIView.animate(withDuration: 1.0, delay: 0.0, options: UIView.AnimationOptions.curveEaseOut, animations: {
                    image.alpha = 0.0
                }, completion: nil)
            }
            
        }
    }
    
    
}
