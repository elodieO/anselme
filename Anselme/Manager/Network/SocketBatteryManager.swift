//
//  SocketIOBatteryManager.swift
//  Anselme
//
//  Created by digital on 03/03/2019.
//  Copyright © 2019 Elodie Oudot. All rights reserved.
//

import SocketIO
import UIKit
import DJISDK

class SocketBatteryManager :NSObject {
    
    static let shared = SocketBatteryManager()
    
    var batteryWarningStatus:(()->())? = nil
    var batteryEndStatus:(()->())? = nil
    var batteryLevel: Int = 100
    
    func returnBattery(batteryView : UIView){
        
        Timer.scheduledTimer(withTimeInterval: TimeInterval(2.0), repeats: true) { (t) in
            let batteryLevelKey = DJIBatteryKey(param: DJIBatteryParamChargeRemainingInPercent)
            DJISDKManager.keyManager()?.getValueFor(batteryLevelKey!, withCompletion: { [unowned self] (value: DJIKeyedValue?, error: Error?) in
                guard error == nil && value != nil else {
                    return
                }
                SocketIOManager.shared.sendBatteryLevel(level: value!.unsignedIntegerValue)
                batteryView.frame.size.width = CGFloat(value!.unsignedIntegerValue)/10
                
                if(value!.unsignedIntegerValue < 30 && value!.unsignedIntegerValue > 25){
                    self.batteryWarningStatus?()
                }
                
                if(value!.unsignedIntegerValue <= 25){
                    self.batteryEndStatus?()
                }
            })
        }
        
    }
    
}


