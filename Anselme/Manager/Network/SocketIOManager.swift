//
//  SocketIOManager.swift
//  Anselme
//
//  Created by digital on 10/02/2019.
//  Copyright © 2019 Elodie Oudot. All rights reserved.
//

import SocketIO
import UIKit

class SocketIOManager :NSObject {
    
    static let shared = SocketIOManager()
    var acceptingSocketCommands: Bool = true

    var send = 0
    
    var socket: SocketIOClient!
    let manager = SocketManager(socketURL: URL(string: "https://mae.suriteka.website")!, config: [.log(false), .compress, .forcePolling(true), .forceNew(true)])
    
    var didReceiveDataCallback:((Data)->())? = nil
    var didIConnectCallback:((Bool)->())? = nil
    var didPatientConnectCallback:((Bool)->())? = nil
    var didAllMembersConnectedCallback:((Bool)->())? = nil
    var receiveInvitationCallback:(()->())? = nil
    var patientAnswerInvitationCallback:((Bool)->())? = nil
    var patientReadyToTakeOffCallback:((Bool)->())? = nil
    var actionSparkDirectionHorizontalRequestCallback:((String)->())? = nil
    var actionSparkRotationRequestCallback:((String)->())? = nil
    var actionSparkEventRequestCallback:((String)->())? = nil
    var actionSparkFeelingRequestCallback:((String)->())? = nil
    var onNextStepCallback:((String)->())? = nil
    var onEndExperience:(()->())? = nil
    var patientSendInvitationMessage:((String)->())? = nil
    var gimbalRotationRequestCallback:((String)->())? = nil
    
    override init() {
        super.init()
        socket = manager.defaultSocket
    }
    
    func connectSocket() {
        socket.on("connect") { _, _ in
            
            // CONNECTION
            self.socket.emit("connectedFamily")
            
            self.socket.on("connectedFamilyCallback"){ (dataArray, ack) in
                self.didIConnectCallback?(true)
            }
            
            self.socket.on("allMembersConnected"){ (dataArray, ack) in
                self.didAllMembersConnectedCallback?(true)
            }
            
            // DISCONNECTION
        
            self.socket.on("disconnectedPatient"){ (dataArray, ack) in
                self.didPatientConnectCallback?(false)
            }
            
            // INVITATION
            
            self.socket.on("invitationReceived"){ (dataArray, ack) in
                self.receiveInvitationCallback?()
            }
            
            self.socket.on("invitationAnswered"){ (dataArray, ack) in
                if dataArray[0] as? String == "Yes" {
                    self.socket.emit("nextStep", "1")
                }else{
                    self.patientAnswerInvitationCallback?(false)
                }
            }
            
            // NEXT STEP
            
            self.socket.on("nextStep"){ (dataArray, ack) in
                if let step = dataArray[0] as? String{
                    self.onNextStepCallback?(step)
                }
                
            }
            
            
            // READY TO TAKE OFF
            
            self.socket.on("patientReadyToTakeOff") { (dataArray, ack) in
                self.patientReadyToTakeOffCallback?(true)
            }

            
            
            
            self.socket.on("stopEmergency"){ (dataArray, ack) in
                print("stop emergency");
            }


//
//
//
//            self.socket.on("pilotage") { (dataArray, ack) in
//                let direction:String = dataArray[0] as! String
//                print(direction)
//            }
//
            self.socket.on("actionSparkDirectionHorizontal") { (dataArray, ack) in
                let actionName:String = dataArray[0] as! String
                self.actionSparkDirectionHorizontalRequestCallback?(actionName)
            }
//
            self.socket.on("sendGimbalValue") { (dataArray, ack) in
                let gimbalValue:String = dataArray[0] as! String
                print("******* gimbal rotation ********")
                self.gimbalRotationRequestCallback?(gimbalValue)
            }
            
            self.socket.on("actionSparkRotation") { (dataArray, ack) in
                let actionName:String = dataArray[0] as! String
                print("******* action rotation ********")
                self.actionSparkRotationRequestCallback?(actionName)
            }
//
            self.socket.on("actionSparkEvent") { (dataArray, ack) in
                let actionName:String = dataArray[0] as! String
                print("******* action event ********")
                self.actionSparkEventRequestCallback?(actionName)
            }
//
            self.socket.on("actionSparkFeeling") { (dataArray, ack) in
                let actionName:String = dataArray[0] as! String
                print("******* socket on action \(actionName) feeling ********")
                self.actionSparkFeelingRequestCallback?(actionName)
            }
//
//            self.socket.on("emotion") { (dataArray, ack) in
//                let emotionName:String = dataArray[0] as! String
//                print(emotionName)
//            }
//
//            self.socket.on("reation") { (dataArray, ack) in
//                let reactionName:String = dataArray[0] as! String
//                print(reactionName)
//            }
//
//            self.socket.on("action") { (dataArray, ack) in
//                let actionName:String = dataArray[0] as! String
//                print(actionName)
//
//            }
            
            self.socket.on("sendMsgToFamily"){ (dataArray, ack) in
                let messageStatus:String = dataArray[0] as! String
                self.patientSendInvitationMessage?(messageStatus)
            }
//
            self.socket.on("stop") { (dataArray, ack) in
                EventManager.instance.stop()
                
            }
            
            self.socket.on("endExperience") { (dataArray, ack) in
                self.onEndExperience?()
                
            }
            
        }
        
        self.socket.on("disconnect") { _,_ in
//            self.socket.connect()
            self.didIConnectCallback?(false)
            EventManager.instance.emergencyStop()
        }
        
        socket.connect()
        
    }
    
    func answerInvitation(answer:String) {
        self.socket.emit("familyAnswered", answer)
    }

    
    func endExperience(){
        self.socket.emit("endExperience")
    }
    
    func sendSparkConnected() {
        self.socket.emit("sparkConnected", "Spark is connected")
    }
    
    func sendBleConnected() {
        self.socket.emit("bleConnected", "BLE is connected")
    }
    
    func sendSetupDone(){
        self.socket.emit("setupDone", "Setup is done")
    }

    func invitationSentToPatient() {
        self.socket.emit("invitationSentToPatient")
    }
    
    func familyReadyToTakeOff() {
        self.socket.emit("familyReadyToTakeOff", "family is ready to takeoff")
    }
//
//    func patientReadyToLandReset() {
//        print("patient readyness reset")
//        self.socket.emit("patientReadyReset", "patient readyness reset")
//    }
//
    func sendBytes(data: Data) {
        self.socket.emit("video", data)
    }
    
    func feelingsFinished() {
        self.socket.emit("feelingFinished")
    }
    
    func sendFeeling(feeling : String){
        self.socket.emit("actionFeeling", feeling)
    }
    
    func familyPiloting(){
        self.socket.emit("familyPiloting")
    }
    
    func familyWatching(){
        self.socket.emit("familyWatching")
    }
    
    func sendMessage(message : String ){
        self.socket.emit("sendMsgToPatient", message)
    }
    
    func sendBatteryLevel(level : UInt ){
        self.socket.emit("sendLevelBattery", String(level))
    }
    
    func droneLanded(){
        self.socket.emit("droneLanded")
    }

    
}
