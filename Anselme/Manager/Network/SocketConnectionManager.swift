//
//  SocketConnectionManager.swift
//  Anselme
//
//  Created by digital on 27/02/2019.
//  Copyright © 2019 Elodie Oudot. All rights reserved.
//

import SocketIO
import UIKit

class SocketConnectionManager :NSObject {
    
    static let shared = SocketConnectionManager()
    
    var patientStatus: Bool = false
    var familyStatus: Bool = false
    var everyoneConnected: Bool = false
    
    func returnConnections(familyImage : UIImageView, patientImage : UIImageView){
        
        SocketIOManager.shared.didIConnectCallback = { (bool) -> () in
            if(bool){
                self.familyStatus = true
            }else{
                self.everyoneConnected = false
                self.familyStatus = false
            }
            self.showConnectionStatus(familyImage: familyImage, patientImage: patientImage)
        }
        
        SocketIOManager.shared.didPatientConnectCallback = { (bool) -> () in
            if bool {
                self.everyoneConnected = true
                self.patientStatus = true
            }else{
                self.everyoneConnected = false
                self.patientStatus = false
                SocketIOManager.shared.didAllMembersConnectedCallback?(false)
            }
            self.showConnectionStatus(familyImage: familyImage, patientImage: patientImage)
        }
        
        SocketIOManager.shared.didAllMembersConnectedCallback = { (bool) -> () in
            if bool {
                self.everyoneConnected = true
                self.patientStatus = true
            }else{
                self.everyoneConnected = false
            }
            self.showConnectionStatus(familyImage: familyImage, patientImage: patientImage)
        }
        
        self.showConnectionStatus(familyImage: familyImage, patientImage: patientImage)
        
    }
    
    func showConnectionStatus(familyImage : UIImageView, patientImage : UIImageView){
        if self.patientStatus {
            patientImage.image = UIImage(named: "patientPictoConnected")
        }else{
            patientImage.image = UIImage(named: "patientPicto")
        }
        
        if self.familyStatus {
            familyImage.image = UIImage(named: "famillePictoConnected")
        }else{
            familyImage.image = UIImage(named: "famillePicto")
        }
    }
    
}

