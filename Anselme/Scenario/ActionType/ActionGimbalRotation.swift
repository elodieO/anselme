//
//  ActionGimbalRotation.swift
//  Anselme
//
//  Created by digital on 17/02/2019.
//  Copyright © 2019 Elodie Oudot. All rights reserved.
//

import Foundation
import UIKit

struct ActionGimbalRotation{
    
    var rotation: Rotation
    var speed : CGFloat
    
    public enum Rotation:String, CaseIterable {
        case up,down
        
        func value() -> Float {
            switch self {
            case .up: return 90.0
            case .down: return -90.0
            }
        }
    }
}
