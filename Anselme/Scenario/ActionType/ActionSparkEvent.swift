//
//  ActionPhoto.swift
//  Anselme
//
//  Created by digital on 16/02/2019.
//  Copyright © 2019 Elodie Oudot. All rights reserved.
//

import Foundation
import UIKit

struct ActionSparkEvent{
    
    var event: Event
    
    public enum Event:String, CaseIterable {
        case takeOff,landing, stop
        
        func value() -> String {
            switch self {
            case .takeOff: return "takeOff"
            case .landing: return "landing"
            case .stop: return "stop"
            }
        }
    }
    
}
