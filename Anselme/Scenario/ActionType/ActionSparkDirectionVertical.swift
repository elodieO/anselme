//
//  ActionGimbalMove.swift
//  Anselme
//
//  Created by digital on 16/02/2019.
//  Copyright © 2019 Elodie Oudot. All rights reserved.
//

import Foundation
import UIKit

struct ActionSparkDirectionVertical{

    var direction: Direction
    var speed : CGFloat
    
    public enum Direction:String, CaseIterable {
        case top,bottom
        
        func value() -> CGFloat {
            switch self {
            case .top: return 1
            case .bottom: return -1
            }
        }
    }
    
    func description() -> String {
        return "\(direction)"
    }
    
}
