//
//  ActionGimbalEvent.swift
//  Anselme
//
//  Created by digital on 17/02/2019.
//  Copyright © 2019 Elodie Oudot. All rights reserved.
//

import Foundation

struct ActionGimbalEvent{
    
    var event: Event
    
    public enum Event:String, CaseIterable {
        case takePictureFirst, takePictureSecond
        
        func value() -> String {
            switch self {
            case .takePictureFirst: return "takePictureFirst"
            case .takePictureSecond: return "takePictureSecond"
            }
        }
    }
}
