//
//  ActionSparkRotation.swift
//  Anselme
//
//  Created by digital on 17/02/2019.
//  Copyright © 2019 Elodie Oudot. All rights reserved.
//

import Foundation
import UIKit

struct ActionSparkRotation{
    
    var rotation: Rotation
    var speed : CGFloat
    
    public enum Rotation:String, CaseIterable {
        case counterClockwise,clockwise
        
        func value() -> CGFloat {
            switch self {
            case .clockwise: return 1
            case .counterClockwise: return -1
            }
        }
    }
    
}
