//
//  ActionColor.swift
//  Anselme
//
//  Created by digital on 18/02/2019.
//  Copyright © 2019 Elodie Oudot. All rights reserved.
//

import Foundation

struct ActionSparkColor{
    
    var BLEColor: BLEColor
    
    public enum BLEColor:String, CaseIterable {
        case angryRed, happyRainbow, sadBlue, heartPink, tiredPurple, smilingYellow, yes, no, reflecting, understood, repeating, wiz, offLeds
        
        func value() -> String {
            switch self {
                case .angryRed: return "angryRed"
                case .happyRainbow: return "happyRainbow"
                case .sadBlue: return "sadBlue"
                case .heartPink: return "heartPink"
                case .tiredPurple: return "tiredPurple"
                case .smilingYellow: return "smilingYellow"
                case .yes: return "yes"
                case .no: return "no"
                case .reflecting: return "reflecting"
                case .understood: return "understood"
                case .repeating: return "repeating"
                case .wiz: return "wiz"
                case .offLeds: return "offLeds"
            }
        }
    }
}
