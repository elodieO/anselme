////
////  ActionType.swift
////  Anselme
////
////  Created by digital on 16/02/2019.
////  Copyright © 2019 Elodie Oudot. All rights reserved.
////
//
//import Foundation
//
//struct ActionType {
//    var actionTypeName : ActionTypeName
//    
//    public enum ActionTypeName {
//        case gimbalMove,sparkMove(actionName : Direction),photograph
//        
//        func value(actionName : Any) -> Any {
//            switch self {
//            case .gimbalMove: return "ActionGimbalMove"
//            case .sparkMove: return Direction(rawValue: actionName as! String)!
//            case .photograph: return "ActionPhoto"
//            }
//        }
//    }
//
//    
//    func description() -> String {
//        return "\(actionTypeName)"
//    }
//}
//
