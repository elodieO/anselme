//
//  Sequence.swift
//  Anselme
//
//  Created by digital on 17/02/2019.
//  Copyright © 2019 Elodie Oudot. All rights reserved.
//

import UIKit

public struct Sequence {
    
    var duration : CGFloat
    var sequenceList : [Action]
    
}

