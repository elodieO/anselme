//
//  ActionManager.swift
//  Anselme
//
//  Created by digital on 16/02/2019.
//  Copyright © 2019 Elodie Oudot. All rights reserved.
//

import Foundation
import DJISDK

class SequenceManager {
    
    static let instance = SequenceManager()
    var sequences = [Sequence]()
    
    var sequenceFinishedCallback:(()->())? = nil
    
    /* Restart the SequenceManger & clean variables */
    func restart() {
        ActionSequence.instance.content.removeAll()
        sequences = []
        
    }
    
    /* Append a sequence to the sequence list */
    func appendSequence(sequence:Sequence) {
        ActionSequence.instance.content.append(sequence)
        sequences.append(sequence)
    }
    
    /* Play sequence */
    func play() {
        print("*** Playing sequence ***")
        executeSequence()
    }
    
    /* Execute sequence */
    func executeSequence() {
        EventManager.instance.stop()
        // for the forst sequence of the list
        if let sequence = sequences.first{
        
            // for action in sequence action list
            for action in sequence.sequenceList{
                
                // execute the action
                ActionManager.instance.executeAction(action: action)
                
            }
            
            // Timing the sequence : when sequence duration is hover, we remove it from the list and reload the function
            Timer.scheduledTimer(withTimeInterval: TimeInterval(sequence.duration), repeats: false) { (t) in
                if self.sequences.count > 0 {
                    self.sequences.remove(at: 0)
                    self.executeSequence()
                }else{
                    EventManager.instance.stop()
                    
                }
                
                
            }
        
            // if there is no sequence in the sequences list : ask spark to stop and clean the list of sequences
        }else{
            print("sequence finished in loop")
            EventManager.instance.stop()
            restart()
            self.sequenceFinishedCallback?()
        }
    }
    
}
