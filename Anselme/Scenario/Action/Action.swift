//
//  Action.swift
//  Anselme
//
//  Created by digital on 10/02/2019.
//  Copyright © 2019 Elodie Oudot. All rights reserved.
//

import UIKit

/* Action Enum */
enum Action {
    
    // The case defined some parameters about the action
    case sparkDirectionHorizontal(direction: ActionSparkDirectionHorizontal.Direction, speed :CGFloat)
    case sparkDirectionVertical(direction: ActionSparkDirectionVertical.Direction, speed :CGFloat)
    case sparkEvent(event: ActionSparkEvent.Event)
    case sparkRotation(rotation : ActionSparkRotation.Rotation, speed : CGFloat)
    case gimbalRotation(rotation : ActionGimbalRotation.Rotation, speed : CGFloat)
    case gimbalEvent(event: ActionGimbalEvent.Event)
    case sparkColor(BLEColor : ActionSparkColor.BLEColor)
    
    // ActionValues depends on the case of Action and return a structure adapted to the type of action
    var actionValues: Any {
        switch self {
        case let .sparkDirectionHorizontal(direction, speed):
            return ActionSparkDirectionHorizontal(direction : direction, speed: speed)
        case let .sparkDirectionVertical(direction, speed):
            return ActionSparkDirectionVertical(direction : direction, speed: speed)
        case let .sparkEvent(event):
            return ActionSparkEvent(event : event)
        case let .sparkRotation(rotation, speed):
            return ActionSparkRotation(rotation : rotation, speed : speed)
        case let .gimbalRotation(rotation, speed):
            return ActionGimbalRotation(rotation : rotation, speed : speed)
        case let .gimbalEvent(event):
            return ActionGimbalEvent(event : event)
        case let .sparkColor(BLEColor):
            return ActionSparkColor(BLEColor : BLEColor)
        }
    }
    
    // Return the name of the action for switch
    func getActionName() -> Any {
        switch self {
        case let .sparkDirectionHorizontal(direction):
            return direction
        case let .sparkDirectionVertical(direction):
            return direction
        case let .sparkEvent(event):
            return event
        case let .sparkRotation(rotation):
            return rotation
        case let .gimbalRotation(rotation):
            return rotation
        case let .gimbalEvent(event):
            return event
        case let .sparkColor(BLEColor):
            return BLEColor
        }
    }
    
    // Return the type of the action for switch
    func getActionType() -> String{
        switch self {
        case .sparkDirectionHorizontal:
            return ".sparkDirectionHorizontal"
        case .sparkDirectionVertical:
            return ".sparkDirectionVertical"
        case .sparkEvent:
            return ".sparkEvent"
        case .sparkRotation:
            return ".sparkRotation"
        case .gimbalRotation:
            return ".gimbalRotation"
        case .gimbalEvent:
            return ".gimbalEvent"
        case .sparkColor:
            return ".sparkColor"
        }
    }
    
}

