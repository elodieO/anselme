//
//  ActionManager.swift
//  Anselme
//
//  Created by digital on 16/02/2019.
//  Copyright © 2019 Elodie Oudot. All rights reserved.
//

import Foundation
import DJISDK

class ActionManager {
    
    static let instance = ActionManager()
    
    /* The function called to execute some actions */
    func executeAction(action : Action) {
        // When we recognize the type of action, we asked the right manager to execute the action
        switch action.getActionType() {
        case ".sparkDirectionHorizontal":
            MovingManager.instance.executeSparkDirectionHorizontal(action: action.actionValues as! ActionSparkDirectionHorizontal)
        case ".sparkDirectionVertical":
            MovingManager.instance.executeSparkDirectionVertical(action: action.actionValues as! ActionSparkDirectionVertical)
        case ".sparkEvent":
            EventManager.instance.executeSparkEvent(action: action.actionValues as! ActionSparkEvent)
        case ".sparkRotation":
            MovingManager.instance.executeSparkRotation(action: action.actionValues as! ActionSparkRotation)
        case ".gimbalRotation":
            GimbalManager.shared.executeGimbalRotation(action: action.actionValues as! ActionGimbalRotation)
        case ".gimbalEvent":
            EventManager.instance.executeGimbalEvent(action: action.actionValues as! ActionGimbalEvent)
        case ".sparkColor":
            ColorManager.instance.executeSparkColor(action: action.actionValues as! ActionSparkColor)
        default:
            print("Unknown actionType asked")
        }
        
    }

}
