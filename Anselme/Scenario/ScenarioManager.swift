//
//  ActionManager.swift
//  Anselme
//
//  Created by digital on 16/02/2019.
//  Copyright © 2019 Elodie Oudot. All rights reserved.
//

import Foundation
import SwiftyJSON

class ScenarioManager {
    var scenarioJSON:JSON
    var scenarioJSONArray:Array<JSON>
    
    /*
     * Init the scenario by passing the name of scenario
     */
    init(name: String) {
        SequenceManager.instance.restart()
        let jsonFile = JSONManager.init(fileName: name)
        scenarioJSON = jsonFile.decode()
        scenarioJSONArray = scenarioJSON.arrayValue
        sequenceJSON()
    }
    
    /* Foreach block in JSON file, corresponding to a Sequence of Actions */
    func sequenceJSON() {
        
        for sequence in scenarioJSONArray {
            JSONtoSequence(sequence:sequence)
        }
        
    }
    
    /* Get the block informations */
    /* to append a Sequence in the SequenceManager that will play the scenario */
    func JSONtoSequence(sequence:JSON) {
    
        let duration:CGFloat = CGFloat((sequence["duration"].rawString()! as NSString).doubleValue)
        var actionsList:[Action] = []
        
        // For each action we find in sequence['actions'], we lunch the actionCreator
        for action in sequence["actions"].arrayValue {
            let actionType = action["actionType"].rawString()!
            let actionName = action["actionName"].rawString()!
            let speed:CGFloat = CGFloat((action["speed"].rawString()! as NSString).doubleValue)
            let myAction = actionCreator(actionType: actionType, actionName: actionName, speed: speed)
            actionsList.append(myAction)
        }
        
        // When we got all the action of a sequence we add the list to the sequence
        // so that the sequence will know if there is simultaneous actions to do at the same time
        let mySequence = Sequence(duration: duration, sequenceList: actionsList)
        
        // We add the sequence into the SequenceManager
        // We will use this to play the Scenario later
        SequenceManager.instance.appendSequence(sequence: mySequence)

    }

    /* Function to create an action */
    func actionCreator(actionType:String, actionName:String, speed: CGFloat) -> Action {
        
        var action:Action = Action.sparkEvent(event: .stop)
        
        // When we got all the action informations, we create the action after switching on actionType & actionName
        switch actionType {
        case ".sparkDirectionHorizontal":
            switch actionName {
            case ".forward" :
                action = Action.sparkDirectionHorizontal(direction: .forward, speed: speed)
            case ".backward" :
                action = Action.sparkDirectionHorizontal(direction: .backward, speed: speed)
            case ".left" :
                action = Action.sparkDirectionHorizontal(direction: .left, speed: speed)
            case ".right" :
                action = Action.sparkDirectionHorizontal(direction: .right, speed: speed)
            case ".backwardRight" :
                action = Action.sparkDirectionHorizontal(direction: .backwardRight, speed: speed)
            case ".backwardLeft" :
                action = Action.sparkDirectionHorizontal(direction: .backwardLeft, speed: speed)
            case ".forwardRight" :
                action = Action.sparkDirectionHorizontal(direction: .forwardRight, speed: speed)
            case ".forwardLeft" :
                action = Action.sparkDirectionHorizontal(direction: .forwardLeft, speed: speed)
            default:
                print("Unknown ActionName of sparkDirectionHorizontal in actionCreator function")
            }
        case ".sparkDirectionVertical":
            switch actionName {
            case ".top" :
                action = Action.sparkDirectionVertical(direction: .top, speed: speed)
            case ".bottom" :
                action = Action.sparkDirectionVertical(direction: .bottom, speed: speed)
            default:
                print("Unknown ActionName of sparkDirectionVertical in actionCreator function")
            }
        case ".sparkEvent":
            switch actionName {
            case ".takeOff" :
                action = Action.sparkEvent(event: .takeOff)
            case ".landing" :
                action = Action.sparkEvent(event: .landing)
            case ".stop" :
                action = Action.sparkEvent(event: .stop)
            default:
                print("Unknown ActionName of sparkEvent in actionCreator function")
            }
        case ".sparkRotation":
            switch actionName {
            case ".clockwise" :
                action = Action.sparkRotation(rotation: .clockwise, speed: speed)
            case ".counterClockwise" :
                action = Action.sparkRotation(rotation: .counterClockwise, speed: speed)
            default:
                print("Unknown ActionName of sparkRotation in actionCreator function")
            }
        case ".gimbalRotation":
            switch actionName {
            case ".down" :
                action = Action.gimbalRotation(rotation: .down, speed: speed)
            case ".up" :
                action = Action.gimbalRotation(rotation: .up, speed: speed)
            default:
                print("Unknown ActionName of gimbalRotation in actionCreator function")
            }
        case ".gimbalEvent":
            switch actionName {
            case ".takePictureFirst" :
                action = Action.gimbalEvent(event: .takePictureFirst)
            case ".takePictureSecond" :
                action = Action.gimbalEvent(event: .takePictureSecond)
            default:
                print("Unknown ActionName of gimbalEvent in actionCreator function")
            }
        case ".sparkColor":
            switch actionName {
            case ".happyRainbow" :
                action = Action.sparkColor(BLEColor: .happyRainbow)
            case ".sadBlue" :
                action = Action.sparkColor(BLEColor: .sadBlue)
            case ".angryRed" :
                action = Action.sparkColor(BLEColor: .angryRed)
            case ".smilingYellow" :
                action = Action.sparkColor(BLEColor: .smilingYellow)
            case ".heartPink" :
                action = Action.sparkColor(BLEColor: .heartPink)
            case ".tiredPurple" :
                action = Action.sparkColor(BLEColor: .tiredPurple)
            case ".yes" :
                action = Action.sparkColor(BLEColor: .yes)
            case ".no" :
                action = Action.sparkColor(BLEColor: .no)
            case ".reflecting" :
                action = Action.sparkColor(BLEColor: .reflecting)
            case ".understood" :
                action = Action.sparkColor(BLEColor: .understood)
            case ".repeating" :
                action = Action.sparkColor(BLEColor: .repeating)
            case ".wiz" :
                action = Action.sparkColor(BLEColor: .wiz)
            case ".offLeds" :
                action = Action.sparkColor(BLEColor: .offLeds)
            default:
                print("Unknown ActionName of sparkColor in actionCreator function")
            }
        default:
            action = Action.sparkEvent(event: .stop)
        }

        return action
    }
    
    /* Play the scenario */
    func play() {
        SequenceManager.instance.play()
    }
}
