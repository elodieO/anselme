//
//  ViewController.swift
//  Anselme
//
//  Created by digital on 16/02/2019.
//  Copyright © 2019 Elodie Oudot. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    

    @IBAction func goToApplication(_ sender: Any) {
        performSegue(withIdentifier: "goToApplication", sender: nil)
    }
    
    @IBAction func goToExercices(_ sender: Any) {
        performSegue(withIdentifier: "goToExercices", sender: nil)
    }
    
    @IBAction func goToTests(_ sender: Any) {
        performSegue(withIdentifier: "goToTests", sender: nil)
    }
    
}
