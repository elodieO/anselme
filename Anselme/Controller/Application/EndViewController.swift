//
//  EndViewController.swift
//  Anselme
//
//  Created by digital on 05/03/2019.
//  Copyright © 2019 Elodie Oudot. All rights reserved.
//

import UIKit

class EndViewController: UIViewController {

    @IBOutlet weak var familyConnectionStatus: UIImageView!
    @IBOutlet weak var patientConnectionStatus: UIImageView!
    @IBOutlet weak var returnButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        SocketConnectionManager.shared.returnConnections(familyImage: familyConnectionStatus, patientImage: patientConnectionStatus)

        self.returnButton.buttonOrange()
        self.returnButton.roundedCorner()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
