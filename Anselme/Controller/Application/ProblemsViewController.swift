//
//  ProblemsViewController.swift
//  Anselme
//
//  Created by digital on 27/02/2019.
//  Copyright © 2019 Elodie Oudot. All rights reserved.
//

import UIKit

class ProblemsViewController: UIViewController {

    @IBOutlet weak var patientConnectionStatus: UIImageView!
    @IBOutlet weak var familyConnectionStatus: UIImageView!
    @IBOutlet weak var returnButton: UIButton!
    @IBOutlet weak var messageView: UIImageView!
    
    var message : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        SocketConnectionManager.shared.returnConnections(familyImage: familyConnectionStatus, patientImage: patientConnectionStatus)
        self.returnButton.buttonOrange()
        self.returnButton.roundedCorner()
        
        if(message == "message1"){
            messageView.image = UIImage(named: "message1")
        }
        
        if(message == "message2"){
            messageView.image = UIImage(named: "message2")
        }
        
        if(message == "message3"){
            messageView.image = UIImage(named: "message3")
        }
    }
    

    
    @IBAction func returnButtonClicked(_ sender: Any) {
        self.performSegue(withIdentifier: "returnHome", sender: nil)
    }
    
}
