//
//  ChoiceViewController.swift
//  Anselme
//
//  Created by digital on 10/02/2019.
//  Copyright © 2019 Elodie Oudot. All rights reserved.
//

import UIKit
import SocketIO

class WaitingRoomViewController: UIViewController {
    
    @IBOutlet weak var patientConnectionStatus: UIImageView!
    @IBOutlet weak var familyConnectionStatus: UIImageView!
    
    @IBOutlet weak var sendInvitation: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Connection management
        SocketIOManager.shared.connectSocket()
        SocketConnectionManager.shared.returnConnections(familyImage: familyConnectionStatus, patientImage: patientConnectionStatus)
        
        // View management
        self.sendInvitation.buttonOrange()
        self.sendInvitation.roundedCorner()
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(true)

        SocketIOManager.shared.receiveInvitationCallback = { () -> () in
            self.performSegue(withIdentifier: "goToInvitationChoice", sender: nil)
        }
        
        SocketIOManager.shared.onNextStepCallback = { (string) -> () in
            if string == "1"{
                Timer.scheduledTimer(withTimeInterval: TimeInterval(4.0), repeats: false) { (t) in
                    self.performSegue(withIdentifier: "goToInstallProcedure", sender: nil)
                }
            }
        }
        
    }
    
    @IBAction func sendInvitation(_ sender: Any) {
        SocketIOManager.shared.invitationSentToPatient()
        self.performSegue(withIdentifier: "goToWaitingResponseRoom", sender: nil)
    }
    
}
