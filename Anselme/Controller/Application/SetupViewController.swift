//
//  SetupViewController.swift
//  Anselme
//
//  Created by digital on 12/02/2019.
//  Copyright © 2019 Elodie Oudot. All rights reserved.
//

import UIKit
import NetworkExtension
import SystemConfiguration.CaptiveNetwork

class SetupViewController: UIViewController {

    
    @IBOutlet weak var viewSetup: UIView!
    @IBOutlet weak var patientConnectionStatus: UIImageView!
    @IBOutlet weak var familyConnectionStatus: UIImageView!
    
    @IBOutlet weak var adviseLabel: UILabel!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelText: UILabel!
    @IBOutlet weak var imageSteps: UIImageView!
    
    
    var bleConnectionSet:Bool = false
    var sparkConnectionSet:Bool = false
    var allConnectionSet:String = "notSet"
    var sparkSet:String = "notSet"
    var didAllConnectionsSet:(()->())? = nil
    var didSparkConnectionsSet:(()->())? = nil
    var didBluetoothConnectionsSet:(()->())? = nil
    
    var bleTimer:Timer? = nil
    var sparkTimer:Timer? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        SocketConnectionManager.shared.returnConnections(familyImage: familyConnectionStatus, patientImage: patientConnectionStatus)
        
        let tripleTap = UITapGestureRecognizer(target: self, action: #selector(tripleTapAction))
        tripleTap.numberOfTapsRequired = 3
        viewSetup.addGestureRecognizer(tripleTap)
        
        
        self.didAllConnectionsSet = {() -> () in
            if self.allConnectionSet != "alreadySet"{
                if self.sparkConnectionSet && self.bleConnectionSet {
                    Timer.scheduledTimer(withTimeInterval: TimeInterval(6.0), repeats: false) { (t) in
                        SocketIOManager.shared.sendSetupDone();
                    }
                    
                }
            }
            
        }
        
        self.didSparkConnectionsSet = {() -> () in
            self.bleTimer = Timer.scheduledTimer(withTimeInterval: TimeInterval(2.0), repeats: true) { (t) in
                if !self.bleConnectionSet{
                     self.connectBluetooth()
                }else{
                    FeelingManager.instance.executeSparkFeeling(actionName: "wiz")
                    self.bleTimer?.invalidate()
                    UIView.animate(withDuration: 1.0, delay: 0.0, options: UIView.AnimationOptions.curveEaseOut, animations: {
                        self.adviseLabel.alpha = 0.0
                        self.labelTitle.alpha = 0.0
                        self.labelText.alpha = 0.0
                    }, completion: {
                        (finished: Bool) -> Void in
                        //Once the label is completely invisible, set the text and fade it back in
                        self.imageSteps.image = UIImage(named: "setup3")
                        self.adviseLabel.text = "Nous préparons le décollage"
                        self.labelTitle.text = "Profitez de ce moment !"
                        self.labelText.text = "N'hésitez pas à interagir et à communiquer avec votre proche"
                        
                        // Fade in
                        UIView.animate(withDuration: 1.0, delay: 0.0, options: UIView.AnimationOptions.curveEaseIn, animations: {
                            self.adviseLabel.alpha = 1.0
                            self.labelTitle.alpha = 1.0
                            self.labelText.alpha = 1.0
                        }, completion: {
                            (finished: Bool) -> Void in
                            self.didBluetoothConnectionsSet?()
                            FeelingManager.instance.executeSparkFeeling(actionName: "offLeds")
                        })
                    })
                }
            }
        }
        
        self.didBluetoothConnectionsSet = {() -> () in
            self.didAllConnectionsSet?()
            self.allConnectionSet = "alreadySet"
        }
        
        
        SocketIOManager.shared.onNextStepCallback = { (string) -> () in
            if string == "2" {
                self.performSegue(withIdentifier: "goToControl", sender: nil)
            }
            
        }
        
        
        self.sparkTimer = Timer.scheduledTimer(withTimeInterval: TimeInterval(2.0), repeats: true) { (t) in
            if !self.sparkConnectionSet{
                self.connectSpark()
            }else{
                self.sparkTimer?.invalidate()
            }
        }
        
        
    }

    func connectBluetooth() {
        
        BluetoothManager.shared.deviceConnection()
        BluetoothManager.shared.didDeviceConnectedCallback = { () -> () in
            SocketIOManager.shared.sendBleConnected();
            self.bleConnectionSet = true
            
        }
    }
    
    func connectSpark(){
        
        ConnectionManager.shared.tryConnection()
        
        ConnectionManager.shared.didSparkConnectedCallback = { () -> () in
            self.sparkConnectionSet = true
            
            if self.sparkSet != "alreadySet"{
                UIView.animate(withDuration: 1.0, delay: 0.0, options: UIView.AnimationOptions.curveEaseOut, animations: {
                    self.adviseLabel.alpha = 0.0
                    self.labelTitle.alpha = 0.0
                    self.labelText.alpha = 0.0
                }, completion: {
                    (finished: Bool) -> Void in
                    
                    //Once the label is completely invisible, set the text and fade it back in
                    self.imageSteps.image = UIImage(named: "setup2")
                    self.adviseLabel.text = "Nous préparons les expressions lumineuses"
                    self.labelTitle.text = "En cas de problème, songez à l'arrêt d'urgence"
                    self.labelText.text = "Appuyez sur le bouton d'arrêt pour couper les moteurs."
                    
                    // Fade in
                    UIView.animate(withDuration: 1.0, delay: 0.0, options: UIView.AnimationOptions.curveEaseIn, animations: {
                        self.adviseLabel.alpha = 1.0
                        self.labelTitle.alpha = 1.0
                        self.labelText.alpha = 1.0
                    }, completion: {
                        (finished: Bool) -> Void in
                        SocketIOManager.shared.sendSparkConnected();
                        
                        
                        self.sparkSet = "alreadySet"
                        self.didSparkConnectionsSet?()
                    })
                    
                    
                })
            }
            
            
            
        }

    }
    
    @objc func tripleTapAction() {
        print("Force bluetooth connection")
        self.connectBluetooth()
    }
    
}
