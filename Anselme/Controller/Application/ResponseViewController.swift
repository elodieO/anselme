//
//  ResponseViewController.swift
//  Anselme
//
//  Created by digital on 27/02/2019.
//  Copyright © 2019 Elodie Oudot. All rights reserved.
//

import UIKit

class ResponseViewController: UIViewController {

    @IBOutlet weak var patientConnectionStatus: UIImageView!
    @IBOutlet weak var familyConnectionStatus: UIImageView!
    @IBOutlet weak var slider: UISlider!
    
    var test :String = "test"

    
    override func viewDidLoad() {
        super.viewDidLoad()
        SocketConnectionManager.shared.returnConnections(familyImage: familyConnectionStatus, patientImage: patientConnectionStatus)
        slider.value = 0.5
        slider.customSliderInvitation()
    }

    @IBAction func sliderResponse(_ sender: Any) {
        
        if SocketConnectionManager.shared.everyoneConnected {
            if self.slider.value == self.slider.maximumValue {
                SocketIOManager.shared.answerInvitation(answer: "Yes")
                self.performSegue(withIdentifier: "goToInstallationProcedure2", sender: nil)
            } else if self.slider.value == self.slider.minimumValue {
                SocketIOManager.shared.answerInvitation(answer: "No")
                _ = navigationController?.popViewController(animated: true)
            }

        }
            
    }
    
    @IBAction func response1clicked(_ sender: Any) {
        SocketIOManager.shared.sendMessage(message: "message1")
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func response2clicked(_ sender: Any) {
        SocketIOManager.shared.sendMessage(message: "message2")
        _ = navigationController?.popViewController(animated: true)
    }
    
    
    
}
