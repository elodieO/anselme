//
//  FeelingViewCell.swift
//  Anselme
//
//  Created by digital on 01/03/2019.
//  Copyright © 2019 Elodie Oudot. All rights reserved.
//

import UIKit

class FeelingViewCell: UICollectionViewCell {
    
    @IBOutlet weak var icon: UIImageView!
}
