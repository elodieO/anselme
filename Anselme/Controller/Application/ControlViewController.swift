//
//  ControlViewController.swift
//  Anselme
//
//  Created by digital on 18/02/2019.
//  Copyright © 2019 Elodie Oudot. All rights reserved.
//

import UIKit
import DJISDK
import VideoPreviewer

class ControlViewController: UIViewController {
    
    let preview = VideoPreviewer()
    
    var dronie: DJIAircraft = DJIAircraft()
    var battery:DJIBatteryState = DJIBatteryState()
    
    @IBOutlet weak var videoView: UIView!
    @IBOutlet weak var frameView: UIImageView!
    
    @IBOutlet weak var waitingPatientView: UIView!
    @IBOutlet weak var takeOffSlider: UISlider!
    @IBOutlet weak var endCall1: UIImageView!
    @IBOutlet weak var waitingLabel1: UILabel!
    @IBOutlet weak var waitingLabel2: UILabel!
    @IBOutlet weak var sliderTakeOffContainer: UIView!
    
    
    
    @IBOutlet weak var pilotView: UIView!
    @IBOutlet weak var gimbalSliderContainer: UIView!
    @IBOutlet weak var gimbalSlide: UISlider!
    @IBOutlet weak var forwardButton: UIView!
    @IBOutlet weak var leftButton: UIView!
    @IBOutlet weak var rightButton: UIView!
    @IBOutlet weak var landingButton: UIView!
    
    @IBOutlet weak var feelingView: UIView!
    @IBOutlet weak var feelingLabel: UILabel!
    @IBOutlet weak var feelingImage: UIImageView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var wizButton: UIView!
    
    
    @IBOutlet weak var globalView: UIView!
    @IBOutlet weak var pilotButton: UIView!
    @IBOutlet weak var feelingButton: UIView!
    @IBOutlet weak var endExperienceButton: UIView!
    @IBOutlet weak var batteryView: UIView!
    
    @IBOutlet weak var patientConnectionStatus: UIImageView!
    @IBOutlet weak var familyConnectionStatus: UIImageView!
    var patientIsReady:Bool = false
    
    
    @IBOutlet weak var batteryWarningView: UIView!
    @IBOutlet weak var batteryEndView: UIView!
    
    var feelingsList:Array = [
        ["yes", #imageLiteral(resourceName: "mini-yes")],
        ["no", #imageLiteral(resourceName: "mini-no")],
        ["understood", #imageLiteral(resourceName: "mini-understood")],
        ["reflecting", #imageLiteral(resourceName: "mini-reflecting")],
        ["repeating", #imageLiteral(resourceName: "mini-repeating")],
        ["smilingYellow", #imageLiteral(resourceName: "mini-smiling")],
        ["heartPink", #imageLiteral(resourceName: "mini-heart")],
        ["happyRainbow", #imageLiteral(resourceName: "mini-happyRainbow")],
        ["tiredPurple", #imageLiteral(resourceName: "mini-tired")],
        ["sadBlue", #imageLiteral(resourceName: "mini-sad")],
        ["angryRed", #imageLiteral(resourceName: "mini-angry")],
        ["leaving", #imageLiteral(resourceName: "mini-leaving")],
        ["followMe", #imageLiteral(resourceName: "mini-followMe")],
//        ["comeCloser", #imageLiteral(resourceName: "mini-comeCloser")],
        ["lookAtMe", #imageLiteral(resourceName: "mini-lookAtMe")]
    ]
    
    var goToEnd:String = "notCalled"
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.collectionViewLayout.collectionView?.delegate = self
        
        // collection view style
        collectionView.layer.backgroundColor = UIColor.clear.cgColor
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .horizontal
        collectionView.collectionViewLayout = flowLayout
        flowLayout.minimumLineSpacing = 0.0
        
        SocketConnectionManager.shared.returnConnections(familyImage: familyConnectionStatus, patientImage: patientConnectionStatus)
        
        showWaitingPatientView()
        
        // Adding button press gesture
        let forwardTap = UILongPressGestureRecognizer(target: self, action: #selector(forwardCommand))
        forwardTap.minimumPressDuration = 0
        forwardButton.addGestureRecognizer(forwardTap)
        
        let clockwiseTap = UILongPressGestureRecognizer(target: self, action: #selector(clockwiseCommand))
        clockwiseTap.minimumPressDuration = 0
        rightButton.addGestureRecognizer(clockwiseTap)
        
        let counterClockwiseTap = UILongPressGestureRecognizer(target: self, action: #selector(counterClockwiseCommand))
        counterClockwiseTap.minimumPressDuration = 0
        leftButton.addGestureRecognizer(counterClockwiseTap)
        
        let endExpTap = UILongPressGestureRecognizer(target: self, action: #selector(endExpCommand))
        endExpTap.minimumPressDuration = 0
        endExperienceButton.addGestureRecognizer(endExpTap)
        
        let takeControlTap = UILongPressGestureRecognizer(target: self, action: #selector(takeControlOverPatient))
        takeControlTap.minimumPressDuration = 0
        pilotButton.addGestureRecognizer(takeControlTap)
        
        let letControlTap = UILongPressGestureRecognizer(target: self, action: #selector(letControlToPatient))
        letControlTap.minimumPressDuration = 0
        feelingButton.addGestureRecognizer(letControlTap)
        
        let wizTap = UILongPressGestureRecognizer(target: self, action: #selector(wizButtonTaped))
        wizTap.minimumPressDuration = 0
        wizButton.addGestureRecognizer(wizTap)
        
        
        takeOffSlider.value = 0.0
        takeOffSlider.customSliderTakeOff()
        
        gimbalSlide.customSliderGimbal()
        gimbalSliderContainer.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi / 2))
       
       
        
        DispatchQueue.main.async {
            self.preview?.adjustViewSize()
            self.preview?.type = .fullWindow
        }
        
        if let _ = DJISDKManager.product() {
            if let camera = self.getCamera(){
                camera.delegate = self
                self.setupVideoPreview()
            }
        }
        
        
        SocketBatteryManager.shared.returnBattery(batteryView: batteryView)
        SocketActionManager.shared.returnActions()
        SocketActionManager.shared.returnFeelings(label  :self.feelingLabel, image: self.feelingImage)
        
        
        SocketIOManager.shared.patientReadyToTakeOffCallback = { (bool) -> () in
            if bool {
                self.showTakeOffView()
            }
        }
        
        SocketIOManager.shared.onEndExperience = { () -> () in
            self.showEndView()
            
        }
        
        SocketBatteryManager.shared.batteryWarningStatus = { () -> () in
            self.showBatteryWarningView()
        }
        
        SocketBatteryManager.shared.batteryEndStatus = { () -> () in
            EventManager.instance.stop()
            EventManager.instance.emergencyStop()
            self.showBatteryEndView()
        }
        
        self.frameView.alpha = 0.0
        
    }

    
    // Pilotage funcs
    
    @IBAction func landingButton(_ sender: Any) {
        EventManager.instance.stop()
        EventManager.instance.emergencyStop()
        EventManager.instance.landing()
        
        if self.goToEnd != "alreadyCalled"{
            self.goToEnd = "alreadyCalled"
            Timer.scheduledTimer(withTimeInterval: TimeInterval(10.0), repeats: false) { (t) in
                SocketIOManager.shared.droneLanded();
                self.performSegue(withIdentifier: "goToEnd", sender: nil)
            }
        }
        
        
        
    }
    
    @IBAction func stopEmergency(_ sender: Any) {
        EventManager.instance.stop()
        EventManager.instance.emergencyStop()
        EventManager.instance.landing()
    }
    
    
    func getCamera() -> DJICamera? {
        if let mySpark = DJISDKManager.product() as? DJIAircraft {
            return mySpark.camera
        }
        return nil
    }
    
    func setupVideoPreview() {
        preview?.setView(self.videoView)
        if let _ = DJISDKManager.product(){
            DJISDKManager.videoFeeder()?.primaryVideoFeed.add(self, with: nil)
        }
        preview?.start()

        DispatchQueue.main.async {
            self.preview?.adjustViewSize()
            self.preview?.type = .fullWindow
        }
        
    }
    
    func resetVideoPreview() {
        preview?.unSetView()
        DJISDKManager.videoFeeder()?.primaryVideoFeed.remove(self)
    }
    
    
    
    @IBAction func takeOffSliderMoved(_ sender: Any) {
        if self.takeOffSlider.value == self.takeOffSlider.maximumValue {
            SocketIOManager.shared.familyReadyToTakeOff()
            self.showFeelingsView()
            EventManager.instance.takeOff()
        }
    }
    
    
    
    func showWaitingPatientView(){
        
        self.pilotView.alpha = 0.0
        self.feelingView.alpha = 0.0
        self.waitingPatientView.alpha = 1.0
        self.globalView.alpha = 0.0
        self.batteryWarningView.alpha = 0.0
        self.batteryEndView.alpha = 0.0
        self.landingButton.alpha = 0.0
        
        self.sliderTakeOffContainer.alpha = 0.0
        self.waitingLabel1.alpha = 1.0
        self.waitingLabel2.alpha = 1.0
        
    }
    
    func showTakeOffView(){
        
        self.pilotView.alpha = 0.0
        self.feelingView.alpha = 0.0
        self.waitingPatientView.alpha = 1.0
        self.globalView.alpha = 0.0
        
        self.sliderTakeOffContainer.alpha = 1.0
        self.waitingLabel1.alpha = 0.0
        self.waitingLabel2.alpha = 0.0
        
    }
    
    func showFeelingsView(){
        self.pilotView.alpha = 0.0
        self.feelingView.alpha = 1.0
        self.waitingPatientView.alpha = 0.0
        self.globalView.alpha = 1.0
        self.feelingButton.isHidden = true
        self.feelingButton.alpha = 0.0
        self.pilotButton.isHidden = false
        self.pilotButton.alpha = 1.0
    }
    
    
    func showPilotView(){
        self.pilotView.alpha = 1.0
        self.feelingView.alpha = 0.0
        self.waitingPatientView.alpha = 0.0
        self.globalView.alpha = 1.0
        self.feelingButton.isHidden = false
        self.feelingButton.alpha = 1.0
        self.pilotButton.isHidden = true
        self.pilotButton.alpha = 0.0
    }
    
    func showBatteryWarningView(){
        self.batteryWarningView.alpha = 1.0
        self.batteryEndView.alpha = 0.0
    }
    
    func showBatteryEndView(){
        self.batteryWarningView.alpha = 0.0
        self.batteryEndView.alpha = 1.0
        SocketIOManager.shared.familyPiloting()
        showPilotView()
        EventManager.instance.stop()
        self.landingButton.alpha = 1.0
        self.globalView.alpha = 0.0
    }
    
    func showEndView(){
        self.batteryWarningView.alpha = 0.0
        self.batteryEndView.alpha = 0.0
        self.globalView.alpha = 0.0
        SocketIOManager.shared.familyPiloting()
        showPilotView()
        EventManager.instance.stop()
        self.landingButton.alpha = 1.0
    }
    
    @IBAction func sendMessage1(_ sender: Any) {
         // TO CHANGE
        SocketIOManager.shared.sendMessage(message: "message3")
    }
    
    @IBAction func sendMessage2(_ sender: Any) {
         // TO CHANGE
        SocketIOManager.shared.sendMessage(message: "message4")
    }
    
    @objc func forwardCommand(gesture: UITapGestureRecognizer) {
        
        // handle touch down and touch up events separately
        if gesture.state == .began {
            MovingManager.instance.directionHorizontal(direction: 1.0)
            
        } else if  gesture.state == .ended {
            EventManager.instance.stop()
        }
    }
    
    @objc func clockwiseCommand(gesture: UITapGestureRecognizer) {
        
        // handle touch down and touch up events separately
        if gesture.state == .began {
            MovingManager.instance.rotate(rotation: 90.0)
        } else if  gesture.state == .ended {
            EventManager.instance.stop()
        }
    }
    
    @objc func counterClockwiseCommand(gesture: UITapGestureRecognizer) {
        
        // handle touch down and touch up events separately
        if gesture.state == .began {
            MovingManager.instance.rotate(rotation: -90.0)
        } else if  gesture.state == .ended {
            EventManager.instance.stop()
        }
    }
    
    @objc func letControlToPatient(gesture: UITapGestureRecognizer) {
        
        if gesture.state == .began {
            SocketIOManager.shared.familyWatching()
            showFeelingsView()
        }
    }
    
    @objc func takeControlOverPatient(gesture: UITapGestureRecognizer) {
        
        if gesture.state == .began {
            SocketIOManager.shared.familyPiloting()
            showPilotView()
            EventManager.instance.stop()
        }
    }
    
    @objc func endExpCommand(gesture: UITapGestureRecognizer) {
        
        if gesture.state == .began {
            SocketIOManager.shared.endExperience()
            self.showEndView()
        }
    }
    
    
    @IBAction func sliderMoved(_ sender: Any) {
        let x = gimbalSlide.value
        let y = -180 * x + 90
        if (y >= -90){
            GimbalManager.shared.rotate(degrees: Float(y), speed: 0.7)
        }
        
    }
    
    @objc func wizButtonTaped(gesture: UITapGestureRecognizer) {
        
        if gesture.state == .began {
            SocketIOManager.shared.sendFeeling(feeling : "wiz")
        }
    }

    func commandsEnabled(val : Bool){
        if val{
            self.collectionView.alpha = 1.0
            self.wizButton.alpha = 1.0
        
        } else {
            self.collectionView.alpha = 0.5
            self.wizButton.alpha = 0.5
        }
    }
    
}

extension ControlViewController:DJIVideoFeedListener {
    func videoFeed(_ videoFeed: DJIVideoFeed, didUpdateVideoData videoData: Data) {
        
        SocketIOManager.shared.sendBytes(data: videoData)
        videoData.withUnsafeBytes { (bytes:UnsafePointer<UInt8>) in
            preview?.push(UnsafeMutablePointer(mutating: bytes), length: Int32(videoData.count))
        }
        
    }
    
    
}

extension ControlViewController:DJISDKManagerDelegate {
    func appRegisteredWithError(_ error: Error?) {
        
    }
    
    
}

extension ControlViewController:DJICameraDelegate {
    
}

extension ControlViewController:UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let feelingName = "\(feelingsList[indexPath.item][0])"
//        if !self.feelingIsPlaying {
            SocketIOManager.shared.sendFeeling(feeling : feelingName)
            self.commandsEnabled(val: false)
            frameView.image = UIImage(named: "frame-"+feelingName)
        
            UIView.animate(withDuration: 1.0, delay: 0.0, options: UIView.AnimationOptions.curveEaseIn, animations: {
                self.frameView.alpha = 1.0
            }, completion: nil)
        
        
            Timer.scheduledTimer(withTimeInterval: TimeInterval(2.0), repeats: false) { (t) in
                
                UIView.animate(withDuration: 1.0, delay: 0.0, options: UIView.AnimationOptions.curveEaseOut, animations: {
                    self.frameView.alpha = 0.0
                    self.commandsEnabled(val: true)
                }, completion: nil)

            }
        
//            SocketIOManager.shared.actionSparkFeeling(feelingName: feelingName)
//            self.commandsEnabled(val: false)
//        }
    }
    
}

extension ControlViewController:UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.width / 5.5
        let height = collectionView.frame.height
        return CGSize(width: width, height: height)
    }
}

extension ControlViewController:UICollectionViewDataSource{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return feelingsList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "feelingCell", for: indexPath) as! FeelingViewCell
        
        cell.icon?.image = feelingsList[indexPath.item][1] as? UIImage
        
        return cell
    }
    
}
