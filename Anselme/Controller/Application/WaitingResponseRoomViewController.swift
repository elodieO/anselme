//
//  WaitingResponseRoomViewController.swift
//  Anselme
//
//  Created by digital on 21/02/2019.
//  Copyright © 2019 Elodie Oudot. All rights reserved.
//

import UIKit

class WaitingResponseRoomViewController: UIViewController {

    @IBOutlet weak var patientConnectionStatus: UIImageView!
    @IBOutlet weak var familyConnectionStatus: UIImageView!
    
    var myTimer : Timer? = nil
    let messages : [String] = ["Ils ne devraient pas tarder", "Attendons encore un peu...", "Attendons leur message ensemble !"]
    var index:Int = 0
    var stockedMessage:String = ""
    
    @IBOutlet weak var waitingMessage: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        SocketConnectionManager.shared.returnConnections(familyImage: familyConnectionStatus, patientImage: patientConnectionStatus)
        
        
        SocketIOManager.shared.patientSendInvitationMessage = {(string) -> () in
            self.stockedMessage = string
        }
        
        SocketIOManager.shared.onNextStepCallback = { (string) -> () in
             if string == "1" {
                self.performSegue(withIdentifier: "goToInstallProcedureAfterWait", sender: nil)
            }
        }
        
        
        SocketIOManager.shared.patientAnswerInvitationCallback = {(bool) -> () in
            if bool == false {
                self.performSegue(withIdentifier: "invitationAnswerNegative", sender: nil)
            }
        }
        
        myTimer = Timer.scheduledTimer(withTimeInterval: TimeInterval(6.0), repeats: true) { (t) in
            
            if(self.index <= self.messages.count - 1){
                self.waitingMessage.text = self.messages[self.index]
                self.index += 1
            }else{
//                self.updateTimer(timer: self.myTimer!)
                self.index = 0
            }
    
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "invitationAnswerNegative"
        {
            let vc = segue.destination as? ProblemsViewController
            vc?.message = stockedMessage
            
        }
    }
    

    func updateTimer(timer: Timer){
        timer.invalidate()
    }

}
