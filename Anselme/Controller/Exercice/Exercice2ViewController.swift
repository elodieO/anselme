//
//  Exercice2ViewController.swift
//  Anselme
//
//  Created by digital on 16/02/2019.
//  Copyright © 2019 Elodie Oudot. All rights reserved.
//

import UIKit

class Exercice2ViewController: UIViewController {

    @IBOutlet weak var deviceStatus: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func connectDevice(_ sender: Any) {
        ConnectionManager.shared.tryConnection()
        ConnectionManager.shared.didSparkConnectedCallback = { () -> () in
            print("spark connected")
            self.deviceStatus.text = "Connected"
        }
    }
    
}
