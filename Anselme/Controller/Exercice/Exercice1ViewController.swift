//
//  Exercice1ViewController.swift
//  Anselme
//
//  Created by digital on 16/02/2019.
//  Copyright © 2019 Elodie Oudot. All rights reserved.
//

import UIKit
import DJISDK
import VideoPreviewer

class Exercice1ViewController: UIViewController {

    @IBOutlet weak var deviceStatus: UILabel!
    var scenario:ScenarioManager!
    
    let prev1 = VideoPreviewer()
    @IBOutlet weak var videoView: UIView!
    
    @IBOutlet weak var imageView1: UIImageView!
    @IBOutlet weak var imageView2: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        SocketIOManager.shared.connectSocket()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func resetVideoPreview() {
        prev1?.unSetView()
        DJISDKManager.videoFeeder()?.primaryVideoFeed.remove(self)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if let camera = self.getCamera() {
            camera.delegate = nil
        }
        self.resetVideoPreview()
    }
    
    func getCamera() -> DJICamera? {
        // Check if it's an aircraft
        if let mySpark = DJISDKManager.product() as? DJIAircraft {

            return mySpark.camera
        }
        
        return nil
    }
    
    
    func captureImage(view: UIImageView){
        print("capture img function")
            self.prev1?.snapshotThumnnail { (image) in
                if let img = image {
                    print(img.size)
                    // Resize it and put it in a neural network! :)
                    
                    view.image = img
                    
                }
            }
        
    }
    
    func setupVideoPreview() {
        print("setup video view")
        prev1?.setView(self.videoView)
        if let _ = DJISDKManager.product(){
            DJISDKManager.videoFeeder()?.primaryVideoFeed.add(self, with: nil)
        }
        prev1?.start()
    }
    
    // Connect to spark
    @IBAction func connectDevice(_ sender: Any) {
        ConnectionManager.shared.tryConnection()
        ConnectionManager.shared.didSparkConnectedCallback = { () -> () in
            print("spark connected")
            self.deviceStatus.text = "Connected"
            
            if let _ = DJISDKManager.product() {
                print("dji product")
                if let camera = self.getCamera(){
                    
                    camera.delegate = self
                    self.setupVideoPreview()
                }
            }
            
        }
    }
    
    // Lunch Scenario
    @IBAction func lunchExercice(_ sender: Any) {
        scenario = ScenarioManager(name: "exercice2")
        scenario.play()
        
        EventManager.instance.takePhotoFirstCallback = { () -> () in
            print("take picture first !")
            self.captureImage(view: self.imageView1)
        }
        
        EventManager.instance.takePhotoSecondCallback = { () -> () in
            print("take picture second !")
            self.captureImage(view: self.imageView2)
        }

    }
    
    // Ask spark to land
    @IBAction func landing(_ sender: Any) {
        EventManager.instance.landing()
    }
    
    // Ask spark to takeOff
    @IBAction func takeOff(_ sender: Any) {
        EventManager.instance.takeOff()
    }
    
    // Ask spark to stop
    @IBAction func stop(_ sender: Any) {
       EventManager.instance.emergencyStop()
    }
}

extension Exercice1ViewController:DJIVideoFeedListener {
    func videoFeed(_ videoFeed: DJIVideoFeed, didUpdateVideoData videoData: Data) {
        
//        SocketIOManager.shared.sendBytes(data: videoData)
//        
//        videoData.withUnsafeBytes { (bytes:UnsafePointer<UInt8>) in
//            prev1?.push(UnsafeMutablePointer(mutating: bytes), length: Int32(videoData.count))
//        }
        
    }
    
    
}

extension Exercice1ViewController:DJISDKManagerDelegate {
    func appRegisteredWithError(_ error: Error?) {
        
    }
    
    
}

extension Exercice1ViewController:DJICameraDelegate {
    
}
