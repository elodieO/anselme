# Mae
iOS App — Mae application, exercices & tests

## Architecture

![Image](https://imgur.com/a/FsX8FE2)

https://imgur.com/a/FsX8FE2

## Codes
This project is the code for the iPhone that control the drone.

Server : https://bitbucket.org/elodieO/maeserver/src

Patient app : https://bitbucket.org/elodieO/maepatient/src

## Exercices

To see exercices, lunch the app and select Exercices.
All the JSON scenarios for exercices are in Anselme/Scenario/Data
Scenarios are lunched in corresponding ViewController in Anselme/Controller/Exercices

Videos : https://www.youtube.com/playlist?list=PLRjR1-0k_4swVZawVdHsnaWpcQ58BcR5B
Same videos as Octave's team - Suriteka repo

## Config

At the root of the project, there is a ```config.json```.
You can pass the application to the mode debug

```
{
"debug": "true"
}
```

## Creation of the JSON

This is an example of the JSON you need to have to manipulate the drone.

```
[
{
"duration" : "6",
"actions" : [
{
"actionType" : ".sparkEvent",
"actionName" : ".takeOff"
}
]
},
{
"duration" : "4",
"actions" : [
{
"actionType" : ".sparkDirectionVertical",
"actionName" : ".top",
"speed" : "0.25"
},
{
"actionType" : ".sparkRotation",
"actionName" : ".clockwise",
"speed" : "0.78"
}
]
}
]
```

### Parameters

#### Spark Horizontal Direction
To create an action that move the drone horizontaly, you need to create ```"actionType" : ".sparkDirectionHorizontal"```
For ```actionTypeName``` there is these values.


|     Parameters     |
|---------------    |
| .forward           |
| .backward          |
| .left              |
| .right             |
| .forwardLeft       |
| .forwardRight      |
| .backwardLeft      |
| .backwardRight     |

#### Spark Vertical Direction
To create an action that move the drone horizontaly, you need to create ```"actionType" : ".sparkDirectionVertical"```
For ```actionTypeName``` there is these values.


|     Parameters     |
|---------------    |
| .top           |
| .bottom          |


#### Spark Rotation
To create an action that change the rotation of the drone, you need to create ```"actionType" : ".sparkRotation"```
For ```actionTypeName``` there is these values.


|     Parameters     |
|---------------    |
| .clockwise           |
| .counterClockwise |


#### Spark Event
To create an action that move the drone horizontaly, you need to create ```"actionType" : ".sparkEvent"```
For ```actionTypeName``` there is these values.

|     Parameters     |
|---------------    |
| .landing           |
| .takeOff          |
| .stop            |


#### Gimbal Rotation
To create an action that move the drone horizontaly, you need to create ```"actionType" : ".gimbalRotation"```
For ```actionTypeName``` there is these values.

|     Parameters     |
|---------------    |
| .up           |
| .down          |

#### Gimbal Event
To create an action that move the drone horizontaly, you need to create ```"actionType" : ".gimbalEvent"```
For ```actionTypeName``` there is these values.

|     Parameters     |
|---------------    |
| .takePictureFirst      |
| .takePictureSecond      |

#### Spark  Color
To create an action that move the drone horizontaly, you need to create ```"actionType" : ".sparkColor"```
For ```actionTypeName``` there is these values.

|     Parameters     |
|---------------    |
| .happyRainbow      |
| .angryRed      |
| .sadBlue      |
| .heartPink      |
| .tiredPurple      |
| .smilingYellow      |
| .yes      |
| .no      |
| .reflecting      |
| .understood      |
| .repeating      |
| .wiz      |
| .offLeds      |
